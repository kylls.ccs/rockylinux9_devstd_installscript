#!/bin/bash

# For Rocky Linux 9 install_script

# update the system

sudo dnf update -y



# install git

sudo dnf install git -y


# setting git
git config --global user.name "$USRNAME"
git config --global user.email "$(whoami)@dev.com"


# install Epel repository

sudo dnf install epel-release -y



# install htop -- system info with terminal

sudo dnf install htop -y



# install chrome

sudo dnf install wget -y

wget https://dl.google.com/linux/linux_signing_key.pub

sudo rpm --import linux_signing_key.pub

wget https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm

sudo dnf install google-chrome-stable_current_x86_64.rpm -y

rm -rf google-chrome-stable_current_x86_64.rpm



# install brave browser

sudo rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc

sudo dnf config-manager --add-repo https://brave-browser-rpm-release.s3.brave.com/x86_64/

sudo dnf install brave-browser -y



# install nvm

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash

export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"

[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm



# install node

nvm install v16.15.0



# install vs code

sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc

printf "[vscode]\nname=packages.microsoft.com\nbaseurl=https://packages.microsoft.com/yumrepos/vscode/\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc\nmetadata_expire=1h" | sudo tee -a /etc/yum.repos.d/vscode.repo

sudo dnf install code -y



# install python-pip

sudo dnf -y install python3-pip



# install powerline

sudo pip3 install powerline-status

sudo pip3 install git+https://github.com/powerline/powerline.git@develop

git clone https://github.com/powerline/fonts.git

cd fonts

./install.sh

cd ..

rm -rf fonts



# install npm && typescript

npm install -g npm@latest

npm install -g typescript





#add anydesk repo & install anydesk

sudo tee /etc/yum.repos.d/anydesk.repo<<EOF

[anydesk]

name=AnyDesk CentOS - stable

baseurl=http://rpm.anydesk.com/centos/x86_64/

gpgcheck=1

repo_gpgcheck=1

gpgkey=https://keys.anydesk.com/repos/RPM-GPG-KEY

EOF



sudo dnf install -y anydesk



# install .NETCLI

curl -sLO https://dot.net/v1/dotnet-install.sh

chmod +x dotnet-install.sh

./dotnet-install.sh -c Current

echo 'export PATH=$PATH:~/.dotnet/' | tee -a ~/.bashrc



# install docker
sudo dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
sudo dnf remove docker docker-common docker-selinux docker-engine -y
sudo dnf -y install device-mapper-persistent-data lvm2
sudo dnf install docker-ce docker-ce-cli containerd.io docker-compose-plugin --allowerasing -y

# -- start docker services
sudo systemctl start docker
sudo systemctl enable docker

# modify docker permission
sudo chmod 666 /var/run/docker.sock

# create project folder
mkdir projects
cd ..

# install only office
sudo dnf groupinstall "Development Tools" -y
wget https://download.onlyoffice.com/install/desktop/editors/linux/onlyoffice-desktopeditors.x86_64.rpm
sudo dnf install onlyoffice-desktopeditors.x86_64.rpm -y

# install thunderbird
sudo dnf install thunderbird -y

# install paint
sudo dnf install -y kolourpaint.x86_64

# update kernel
sudo dnf update --refresh -y
sudo dnf install dkms kernel-devel kernel-headers gcc make bzip2 perl elfutils-libelf-devel -y


reboot